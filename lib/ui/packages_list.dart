import 'package:flutter/material.dart';
import 'package:ktv_app/utils/json_helper.dart';
import 'dart:convert';


const Color PURPLE = Color(0xFF8c77ec);
JsonHelper jsonHelper = new JsonHelper();

class PackagesList extends StatefulWidget{
  final packageListPath;
  final package;
  //String packagePath ="data/json/";
  PackagesList({Key key,this.packageListPath,this.package})  : super(key:key);
  @override
  _PackagesListState createState() => _PackagesListState();

}

class _PackagesListState  extends State<PackagesList>{

  var packagesChannelsList = List();

  @override
  void initState() {
    _loadList();
    super.initState();
  }

  _loadList() async{
    var  list = await jsonHelper.loadJson(widget.packageListPath) ;
    print(list);
    if (list != null) {
      setState(() {
        packagesChannelsList = json.decode(list) ;
        //print(packagesChannelsList);
      });
    } else {
      throw Exception('Failed to load posts');
    }
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title:Text(widget.package['package_name']) ,
        backgroundColor:PURPLE,
      ),
      body: ListView.builder(
        itemCount: packagesChannelsList.length,
        itemBuilder: (BuildContext context, int index) {
          final data = packagesChannelsList[index];
          return ListTile(
            leading: Icon(Icons.tv),
            title: Text(data["channel_name"]),
          );
        }
      ),
        floatingActionButton : FloatingActionButton.extended(
            onPressed: null,
            icon: Icon(Icons.local_mall),
            label: Text("Channels : ${packagesChannelsList.length} ")),

    );
  }

}