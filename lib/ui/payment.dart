import 'package:http/http.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:io';
import 'dart:async';

class Payment extends StatefulWidget {

  @override
  _PaymentState createState() => _PaymentState();

}

class _PaymentState extends State<Payment> {
  @override

  Map<String, String> body = {
    "amount": "100", //amount to be paid
    "purpose": "Advertising",
    "buyer_name": "das",
    "email": "A@gmail.com",
    "phone": "789456132",
    "allow_repeated_payments": "true",
    "send_email": "false",
    "send_sms": "false",
    "redirect_url": "http://www.example.com/redirect/",
    //Where to redirect after a successful payment.
    "webhook": "http://www.example.com/webhook/",
  };



  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Payment testing'),
      ),
      body: Center(
        child:  FloatingActionButton.extended(
            onPressed: () async {

              Map mapMerchant =
              {
                "key":"zKljEZS0",
                "salt" : "xMdHZv22Yf",
                "Auth Header" : "RS0lAVR1iM2EALqlf6Q8DBR99kL+XM7udCxumx2wDb0=",
                "txnid" : "b728690de9f3635d5671",
                "name":"anil",
                "email":"nishant7.ng@gmail.com",
                "amount" : "100",
                "phone" : "8692947192",
                "productInfo" : "cable recharge",
                "surl" : "www.google.com",
                "furl" : "www.facebook.com",
              };


              //String url = 'https://private-anon-3ee604bef4-payu21.apiary-mock.com/pl/standard/user/oauth/authorize';
              String url = '"https://sandboxsecure.payu.in/_payment';
              print(await apiRequest(url, mapMerchant));
            },
            icon: Icon(Icons.payment),
            label: Text("Pay you")),
      ),
      );

  }



  Future<String> apiRequest(String url, Map jsonMap) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', 'Bearer 3e5cac39-7e38-4139-8fd6-30adc06a61bd');
    request.headers.set('Content-type', 'application/json');
    request.add(utf8.encode(json.encode(jsonMap)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    return reply;
  }

}