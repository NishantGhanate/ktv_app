import 'dart:convert';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ktv_app/utils/json_helper.dart';
import 'package:ktv_app/ui/dialog_handle.dart';



String path = 'data/json/channels.json';

class ChannelsPage extends StatefulWidget {
  @override
  _ChannelsPageState createState() => _ChannelsPageState();
}

class _ChannelsPageState extends State<ChannelsPage> with AutomaticKeepAliveClientMixin<ChannelsPage> {
  // TODO: implement wantKeepAlive
  @override
  bool get wantKeepAlive => true;
  
  @override
  void initState() {
    _loadList();
    super.initState();
    items.addAll(channelNames);
  }
  List<String> channelNames = new List<String>();
  List<bool> _isChecked = new List<bool>();
  DialogHandle dialogHandle = new DialogHandle();
  var channelsList = List();

  _loadList() async {
    JsonHelper jsonHelper = new JsonHelper();

    var  list = await jsonHelper.loadJson(path) ;
    //print(List);
      if (list != null)
        {
        setState(() {channelsList = json.decode(list) ;
          for(int i =0;i<=channelsList.length;i++){
            _isChecked.add(false);
            //channelNames.add(channelsList[i]['channel_name']);
          }
        });
    } else {
      throw Exception('Failed to load posts');
    }
  }
  
  double _price = 0;
  List<String> _selectedChannels = new List<String>();

  void changed(value , index){
    setState(() {
      if(value){
        _isChecked[index] = value;
        _price = _price + double.parse(channelsList[index]['channel_price']) ;
        _selectedChannels.add(channelsList[index]['channel_name']);
      }
      else{
        _isChecked[index] = value;
        _price =  _price - double.parse(channelsList[index]['channel_price']) ;
        _selectedChannels.remove(channelsList[index]['channel_name']);
      }


    });
  }
  TextEditingController searchController = TextEditingController();
  final duplicateItems = List<String>.generate(329, (i) => "Item $i");
  var items = List<String>();

  void filterSearchResults(String query) {
    List<String> dummySearchList = List<String>();
    dummySearchList.addAll(channelNames);
    if(query.isNotEmpty) {
      List<String> dummyListData = List<String>();
      dummySearchList.forEach((item) {
        if(item.contains(query)) {
          print(item);
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(channelNames);

      });
    }

  }

  final rs = "Rs ";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (value) {
                  //filterSearchResults(value);
                },
                controller: searchController,
                decoration: InputDecoration(
                  labelText: "Search",
                  hintText: "Search",
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(1.0)),
                  ),
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                  shrinkWrap: false,
                  itemCount: channelsList.length,
                  itemBuilder: (BuildContext context, int index) {
                    final data = channelsList[index];
                    return CheckboxListTile(
                      title: Text(data['channel_name']),
                      secondary: const Icon(Icons.tv),
                      value: _isChecked[index],
                      subtitle: Text(rs + data['channel_price'],
                        style: TextStyle(fontWeight: FontWeight.bold),),
                      onChanged: (value) {
                        changed(value, index);
                      },
                    );
                  }
              ),
            ),

          ],
        ),
      ),


      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          dialogHandle.getPriceDailog(context, _selectedChannels, _price);
        },
        //label: Text("Show price"),
        tooltip: 'Show the price',
        backgroundColor: Colors.deepPurple,
//        mini: true,
//        child: Icon(Icons.expand_less, color: Colors.white,),
        label: Text( rs +_price.toString()),
        icon: Icon(Icons.expand_less, color: Colors.white,),
      ),

    );
  }

  } // End of ChannelsPage

