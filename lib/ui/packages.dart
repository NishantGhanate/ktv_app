import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:ktv_app/utils/json_helper.dart';
import 'package:ktv_app/ui/packages_list.dart';
//const Color PURPLE = Color(0xFF8c77ec);

String packagesPath = 'data/json/packages.json';
JsonHelper jsonHelper = new JsonHelper();

class Packages extends StatefulWidget {

  @override
  _PackageState createState() => _PackageState();

}

class _PackageState extends State<Packages> with  AutomaticKeepAliveClientMixin<Packages> {
  // TODO: implement wantKeepAlive
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    _loadList();
    super.initState();
  }

  var packagesList = List();

  _loadList() async {

    var  list = await jsonHelper.loadJson(packagesPath) ;
    print(list);
    if (list != null) {
      setState(() {
        packagesList = json.decode(list) ;
        print(packagesList);
      });
    } else {
      throw Exception('Failed to load posts');
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body : GridView.builder(
            itemCount: packagesList.length,
            gridDelegate:
            new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            itemBuilder: (BuildContext context, int index) {
              var packageData = packagesList[index];
              return new GestureDetector(
                child: new Card(

                  shape: new RoundedRectangleBorder(
                      side: new BorderSide(color: Color(0xFF8c77ec), width: 2.0),
                      borderRadius: BorderRadius.circular(4.0)),
                  elevation: 5.0,
                  child: new Container(
                    alignment: Alignment.center,
                    child: new Text(packageData['package_name'] + '\n\nPrice : ' + packageData['package_price'] + 'Rs', style: TextStyle(
                      fontSize: 16,
                    ),),
                  ),
                ),

                onTap: () {
                  print('Item $index');
                  var path =  _getPath(packageData['package_name']);
                  print(path);
                  var route = new MaterialPageRoute(builder: (context) => PackagesList(packageListPath: path , package: packageData));
                  Navigator.of(context).push(route);
                },

              );

            })

    ); // This
  }

  String _getPath(String fpath) {
    var path = fpath.split(" ");
    String newPath = path[2] + path[3];
    newPath = newPath.replaceAll(".", "");
    newPath ="data/json/" +newPath+".json";
    return newPath ;
  }


  
}//

