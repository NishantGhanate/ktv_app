import 'package:flutter/material.dart';
import 'package:ktv_app/models/user_model.dart';
import 'package:ktv_app/utils/database_helper.dart';
import 'package:http/http.dart' as http;

class DialogHandle{
  var db = new DatabaseHelper();
  TextEditingController stbController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  getPriceDailog(BuildContext context,List<String> channels,double channelPrice){
    var gst18 = (18*100)/ (130 + channelPrice) ;
    print(gst18);
    gst18 = gst18.roundToDouble();
    var gstTotal = gst18 + 130 + channelPrice;
    gstTotal = gstTotal.roundToDouble();
   print(gstTotal);
    return showDialog(
      context: context,
       // barrierDismissible: false,
        builder: (BuildContext context){
        return AlertDialog(
          title: Text('Total Rs $gstTotal'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                const SizedBox(height: 24.0),
                Text("Seletected : ${channels.length}"),
                const SizedBox(height: 24.0),
                Text("Price  Rs : $channelPrice "),
                const SizedBox(height: 24.0),
                Text("Base Rs : 130 "),
                const SizedBox(height: 24.0),
                Text("18% GST Rs :  $gst18 "),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Save"),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
          ],
        );

      }
    );
  }// Price get handle


  getDetails(BuildContext context) async {

    List userDetails =  await db.getUser(1);
    var stbNumber = UserModel.fromMap(userDetails[0]).user_cable_stb;
    var userAddress = UserModel.fromMap(userDetails[0]).user_address;
    print(stbNumber);
    bool save ;
    // IF we already have record update it else save it
    if(userDetails != null){
      save = false;
      stbController.text = stbNumber;
      addressController.text = userAddress;
    }
    else{
      save = true;
    }

    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Add Details'),
            content: SingleChildScrollView(
              padding: EdgeInsets.all(8.0),
              child: ListBody(
                children: <Widget>[
//                  Text('STB'),
                  const SizedBox(height: 10.0),
                  new Container(
                    width: 300,
                    //height: 20.0,
                    child:TextField(
                      keyboardType: TextInputType.number,
                      obscureText: false,
                      controller: stbController,
                      cursorColor: Colors.black,
                      cursorRadius: Radius.circular(5.0),
                      maxLength: 11,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Setup box no.',
                        suffixIcon: Icon(Icons.card_membership),
                      ),
                      maxLines: 1,
                    ),
                  ),
                  new Container(
                    width: 300,
                    child: TextField(
                      keyboardType: TextInputType.text,
                      obscureText: false,
                      controller: addressController,
                      cursorColor: Colors.black,
                      cursorRadius: Radius.circular(5.0),
                      maxLength: 128,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Address',
                        suffixIcon: Icon(Icons.home),
                      ),
                      maxLines: 2,
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("Save"),
                onPressed: () {
                  if(save){_saveDetails(stbController.text,addressController.text);}
                  else{ _updateDetails();}
                },
              ),
              FlatButton(

                child: Text("Send" ,style: TextStyle(color: Colors.green),),
                onPressed: () {
                  _sendDetails();
                },
              ),
              FlatButton(
                child: Text("Cancel" ,style: TextStyle(color: Colors.red),),
                onPressed: () {
                  //passwordController.text='';
                  Navigator.pop(context);
                },
              )
            ],

          );
        }
    );
  }

  void _updateDetails() async{
    print(stbController.text);
    UserModel userModel = new UserModel("",addressController.text ,stbController.text , "", "", "");
    await db.updateUser(userModel);
  }

  void _saveDetails(String stbNumber , String address) async{
    String _user_name = "";
    String _user_address = address;
    String _user_cable_stb = stbNumber;
    String _user_cable_package = "";
    String _user_internet_name = "";
    String _user_internet_package = "" ;
    await db.saveUser(UserModel(
        _user_name,
        _user_address,
        _user_cable_stb,
        _user_cable_package,
        _user_internet_name,
        _user_internet_package
    ));
  }

  void _sendDetails() {
    Map data = {
      'apikey': '12345678901234567890'
    };

    var url = 'http://httpbin.org/post';
    http.post(url, body: data)
        .then((response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.body}");
    });
  }




}