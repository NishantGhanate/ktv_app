class ChannelModel{
  int _id;
  String _channel_icon;
  String _channel_name;
  String _channel_price;
  String _channel_broadcaster;
  String _channel_genre;
  String _channel_language;
  String _channel_quality;

  ChannelModel(
      this._channel_icon,
      this._channel_name,
      this._channel_price,
      this._channel_broadcaster,
      this._channel_genre,
      this._channel_language,
      this._channel_quality,

      );

  ChannelModel.map(dynamic obj){
    this._id=obj['id'];
    this._channel_icon = obj['channel_icon'];
    this._channel_name = obj['channel_name'];
    this._channel_price = obj['channel_price'];
    this._channel_broadcaster = obj['channel_broadcaster'];
    this._channel_genre = obj['channel_genre'];
    this._channel_language = obj['channel_language'];
    this._channel_quality = obj['channel_quality'];
  }

  int get id => _id;
  String get channel_icon => _channel_icon;
  String get channel_name => _channel_name;
  String get channel_price => _channel_price;
  String get channel_broadcaster => _channel_broadcaster;
  String get channel_genre => _channel_genre;
  String get channel_language => _channel_language;
  String get channel_quality => _channel_quality;

  Map<String, dynamic> toMap(){
    var map = new Map<String, dynamic>();
    map["channel_icon"]=_channel_icon;
    map["channel_name"]=_channel_name;
    map["channel_price"]=_channel_price;
    map["channel_broadcaster"]=_channel_broadcaster;
    map["channel_genre"]=_channel_genre;
    map["channel_language"]=_channel_language;
    map["channel_quality"]=_channel_genre;
    if(id!=null){
      map["id"]=_id;
    }
    return map;
  }

  ChannelModel.fromMap(Map<String, dynamic> map) {
    this._id=map["id"];
    this._channel_icon = map['channel_icon'];
    this._channel_name = map['channel_name'];
    this._channel_price = map['channel_price'];
    this._channel_broadcaster = map['channel_broadcaster'];
    this._channel_genre = map['channel_genre'];
    this._channel_language = map['channel_language'];
    this._channel_quality = map['channel_quality'];

  }

}
