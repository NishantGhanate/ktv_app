class UserModel{
  int _id;
  String _user_name;
  String _user_address;
  String _user_cable_stb;
  String _user_cable_package;
  String _user_internet_name;
  String _user_internet_package;

  UserModel(
      this._user_name,
      this._user_address,
      this._user_cable_stb,
      this._user_cable_package,
      this._user_internet_name,
      this._user_internet_package,
      );

  UserModel.map(dynamic obj){
    this._id=obj['id'];
    this._user_name = obj['user_name'];
    this._user_address = obj['user_address'];
    this._user_cable_stb = obj['user_cable_stb'];
    this._user_cable_package = obj['user_cable_package'];
    this._user_internet_name = obj['user_internet_name'];
    this._user_internet_package = obj['user_internet_package'];
  }

  int get id => _id;
  String get user_name => _user_name;
  String get user_address => _user_address;
  String get user_cable_stb => _user_cable_stb;
  String get user_cable_package => _user_cable_package;
  String get user_internet_name => _user_internet_name;
  String get user_internet_package => _user_internet_package;

  Map<String, dynamic> toMap(){
    var map = new Map<String, dynamic>();
    map["user_name"] = _user_name;
    map["user_address"]= _user_address;
    map["user_cable_stb"]= _user_cable_stb;
    map["user_cable_package"]= _user_cable_package;
    map["user_internet_name"]= _user_internet_name;
    map["user_internet_package"]= _user_internet_package;
    if(id!=null){
      map["id"]=_id;
    }
    return map;
  }

  UserModel.fromMap(Map<String, dynamic> map) {
    this._id=map["id"];
    this._user_name = map['user_name'];
    this._user_address = map['user_address'];
    this._user_cable_stb = map['user_cable_stb'];
    this._user_cable_package = map['user_cable_package'];
    this._user_internet_name = map['user_internet_name'];
    this._user_internet_package = map['user_internet_package'];


  }


}