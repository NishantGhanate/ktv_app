import 'dart:async' show Future;
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:path/path.dart';


class JsonHelper{

  Future<String> _loadJsonAsset(path) async {
    return await rootBundle.loadString(path);
  }

  Future loadJson(String path) async {
    String jsonCrossword = await _loadJsonAsset(path);
    return jsonCrossword;
  }



}//End of class

