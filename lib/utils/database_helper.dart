import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../models/channel_model.dart';
import '../models/user_model.dart';

final String user_table = 'userTable';
final String channels_table = 'channelTable';
final String packages_table = 'packagesTable';
final String history_table = 'historyTable';

class DatabaseHelper{
  static final DatabaseHelper _instance = DatabaseHelper.internal();

  factory DatabaseHelper()=>_instance;

  static Database _db;
  Future<Database> get db async{
    if(_db!=null){
      return _db;
    }
    else{
      _db=await initDb();
      return _db;
    }
  }

  DatabaseHelper.internal();

  initDb() async{
    Directory documentDirectory=await getApplicationDocumentsDirectory();
    String path=join(documentDirectory.path,"ktv.db");
    var ourDb=await openDatabase(path,version: 1,onCreate: _onCreate);
    return ourDb;
  }

  void _onCreate(Database db,int newVersion) async{

    await db.execute(
        "CREATE TABLE $user_table('id' INTEGER PRIMARY KEY,'user_name' TEXT,'"
            "user_address' TEXT , 'user_cable_stb' TEXT , 'user_cable_package' TEXT,"
            "'user_internet_name' TEXT,'user_internet_package' TEXT)"

    );

    await db.execute(
        "CREATE TABLE $channels_table('id' INTEGER PRIMARY KEY,"
            "'channel_icon' TEXT, 'channel_name' TEXT, 'channel_price' INTEGER ,"
            "'channel_broadcaster' TEXT , 'channel_genre' TEXT , "
            " 'channel_language' TEXT , 'channel_quality' TEXT )"
    );

    await db.execute(
      "CREATE TABLE $packages_table('id' INTEGER PRIMARY KEY,'username' TEXT,'password' TEXT)"

    );

    await db.execute(
        "CREATE TABLE $history_table('id' INTEGER PRIMARY KEY,'channel_id' TEXT,'password' TEXT)"

    );

  }

  Future<int> saveChannels (ChannelModel channelModel) async{
    var dbClient = await db;
    var res = await dbClient.insert("$channels_table", channelModel.toMap());
    return res;
  }

  Future<List> getAllChannels() async{
    var dbClient = await db;
    var result = await dbClient.rawQuery("SELECT * FROM $channels_table ");
    return result.toList();
  }

  Future<int> getCount() async{
    var dbClient = await db;
    return Sqflite.firstIntValue(
        await dbClient.rawQuery("SELECT COUNT(*) FROM $channels_table")
    );
  }

  //-------------------------- User models ------------------------------------
  Future<int> saveUser (UserModel userModel) async{
    var dbClient = await db;
    var res = await dbClient.insert("$user_table", userModel.toMap());
    return res;
  }

  Future<List> getUser(int id) async{
    var dbClient = await db;
    var result = await dbClient.rawQuery("SELECT * FROM $user_table WHERE id=$id");
    if(result.length == 0) return null;
    return result.toList();
  }

  Future<int> deleteUser(int id) async{
    var dbClient = await db;
    return await dbClient.delete(user_table, where: "id= ?", whereArgs: [id]);
  }

  Future<int> updateUser(UserModel user) async{
    var dbClient = await db;
    return await dbClient.update(user_table, user.toMap(), where: "id= ?", whereArgs: [1]);
  }

  Future close() async{
    var dbClient= await db;
    dbClient.close();
  }
}
