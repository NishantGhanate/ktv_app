import 'package:flutter/material.dart';
import 'package:ktv_app/ui/channels.dart';
import 'package:ktv_app/ui/packages.dart';
import 'package:ktv_app/ui/history.dart';
import 'package:ktv_app/ui/payment.dart';
import 'package:ktv_app/utils/json_helper.dart';
import 'package:ktv_app/ui/dialog_handle.dart';

var channels;

const Color PURPLE = Color(0xFF8c77ec);
Future main() async {

  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {
  TabController tabController;
  DialogHandle dialogHandle = new DialogHandle();

  @override
  void initState() {
    super.initState();
    tabController = new TabController(length: 3, vsync: this);
  }


  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        backgroundColor: PURPLE,
        title: Text("Ktv"),
//        bottom: new TabBar(
//          controller: tabcontroller,
//            tabs: <Widget>[
//              new Tab(icon: new Icon(Icons.tv) , text: 'Channels',),
//              new Tab(icon: new Icon(Icons.add_to_photos ), text: 'Packages', ),
//              new Tab(icon: new Icon(Icons.history) ,  text: 'History'),
//            ],
//        ), //bottom
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              curve: Curves.fastOutSlowIn,
              child: Text('Drawer Header'),
              decoration: BoxDecoration(
                color: PURPLE,
              ),
            ),
            ListTile(
              leading: Icon(Icons.details),
              title: Text('Add Details'),
              onTap: () {
                Navigator.pop(context);
                dialogHandle.getDetails(context);

              },
            ),
            new Divider(),
            ListTile(
              leading: Icon(Icons.info),
              title: Text('Payment'),
              onTap: () {
                Navigator.pop(context);
                var route = new MaterialPageRoute(builder: (context) =>  Payment());
                Navigator.of(context).push(route);
              },
            ),
            new Divider(),
            ListTile(
              leading: Icon(Icons.feedback),
              title: Text('Feedback'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: new Material(
        color: PURPLE,
        child: new TabBar(
          controller: tabController,
          tabs: <Widget>[
            new Tab(icon: new Icon(Icons.tv) , text: 'Channels',),
            new Tab(icon: new Icon(Icons.add_to_photos ), text: 'Packages', ),
            new Tab(icon: new Icon(Icons.history) ,  text: 'History'),
          ],
        ),

      ),
      body: new TabBarView(
          controller: tabController,
        children: <Widget>[
          ChannelsPage(),
          Packages(),
          Icon(Icons.history),
        ],
      ),
    );
  }
}